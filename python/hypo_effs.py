import ROOT, sys

hypos = [
    #"I_dl_0w2h2t_j",
    "I_sl_0w2h2t_q",
    "I_sl_1w2h2t_q",
    "I_sl_2w2h2t_q",
    "I_sl_0w2h2t_j",
    "I_sl_1w2h2t_j",
    "I_sl_2w2h2t_j",
]

inf = ROOT.TFile(sys.argv[1])
tt = inf.Get("tree")

print tt.GetEntries()
for hypo in hypos:
    ROOT.gROOT.cd()
    n = tt.Draw("({0}_mem_time_tth + {0}_mem_time_ttbb) >> h".format(hypo), "{0}_was_calculated==1".format(hypo), "goff batch")
    h = ROOT.gROOT.Get("h")
    mean_time = h.GetMean()/1000.0
    print "{0} was_calc={1} time={2:.2f}".format(hypo, tt.GetEntries("{0}_was_calculated == 1".format(hypo)), mean_time)

for cat in range(11):
    print cat, tt.GetEntries("category == {0}".format(cat))
