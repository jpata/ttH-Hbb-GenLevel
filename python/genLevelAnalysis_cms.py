import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gSystem.Load("libFWCoreFWLite.so")
import sys, os, pickle
import logging
import copy
import numpy as np
import resource
import math

from PhysicsTools.HeppyCore.framework.chain import Chain as Events
from TTH.MEAnalysis.MEMUtils import set_integration_vars, add_obj
from TTH.MEAnalysis.samples_base import getSitePrefix
from PhysicsTools.HeppyCore.framework.analyzer import Analyzer
from PhysicsTools.Heppy.analyzers.core.AutoFillTreeProducer import *
from TTH.GenLevel.genLevelAnalysis import MEMAnalyzer, EventInterpretation, interp_type, conf, genParticleType, genJetType, fillCoreVariables
from PhysicsTools.HeppyCore.utils.deltar import matchObjectCollection
from PhysicsTools.HeppyCore.framework.analyzer import Analyzer
from TTH.MEAnalysis.VHbbTree import *


#maps the sample type to a number
SAMPLE_CODE = {
    "sig": 0,
    "bkg": 1,
}

#maps the gen-level category to a number
CATEGORY_CODE = {
    "2w2h2t": 0,
    "2w1h2t": 1,
    "2w2h1t": 2,
    "1w2h2t": 3,
    "1w1h2t": 4,
    "1w2h1t": 5,
    "0w2h2t": 6,
    "0w1h2t": 7,
    "0w2h1t": 8,
    "2w": 9,
    "1w": 10,
    "0w": 11,
}

class Jet:
    def __init__(self, tree, n):
        self.pt = tree.Jet_pt[n];
        self.eta = tree.Jet_eta[n];
        self.phi = tree.Jet_phi[n];
        self.mass = tree.Jet_mass[n];
    
    @staticmethod
    def make_array(input):
        return [Jet(input, i) for i in range(input.nJet)]

class EventAnalyzer(Analyzer):
    """Creates the event representation from the ROOT TTree. This is copied from VhbbTree.py
    """
    def __init__(self, cfg_ana, cfg_comp, looperName):
        super(EventAnalyzer, self).__init__(cfg_ana, cfg_comp, looperName)
    def process(self, event):
        event.GenBQuarkFromH = GenBQuarkFromH.make_array(event.input)
        event.GenBQuarkFromHafterISR = GenBQuarkFromHafterISR.make_array(event.input)
        event.GenBQuarkFromTop = GenBQuarkFromTop.make_array(event.input)
        event.GenHiggsBoson = GenHiggsBoson.make_array(event.input)
        event.GenJet = GenJet.make_array(event.input)
        event.GenLep = GenLep.make_array(event.input)
        event.GenLepFromTop = GenLepFromTop.make_array(event.input)
        event.GenNuFromTop = GenNuFromTop.make_array(event.input)
        event.GenStatus2bHad = GenStatus2bHad.make_array(event.input)
        event.GenTop = GenTop.make_array(event.input)
        event.GenWZQuark = GenWZQuark.make_array(event.input)
        event.ttCls = getattr(event.input, "ttCls", None)
        event.Jet = Jet.make_array(event.input)

        return True

class Particle:

    @staticmethod
    def from_obj(obj, parent_id=0):
        p4 = ROOT.TLorentzVector()
        p4.SetPtEtaPhiM(obj.pt, obj.eta, obj.phi, obj.mass)
        numBHadrons = -99
        numCHadrons = -99
        if hasattr(obj, "numBHadrons"):
            numBHadrons = obj.numBHadronsFromTop + obj.numBHadronsAfterTop + obj.numBHadrons
        if hasattr(obj, "numCHadrons"):
            numCHadrons = obj.numCHadronsFromTop + obj.numCHadronsAfterTop + obj.numCHadrons
        return Particle(
            _p4 = p4,
            _pdg_id = getattr(obj, "pdgId", -99),
            _status = getattr(obj, "status", -99),
            numBHadrons = numBHadrons,
            numCHadrons = numCHadrons,
            _parent_id = parent_id,
            source = "unknown"
        )

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def is_final_state(self):
        return True

    def pdg_id(self):
        return self._pdg_id

    def p4(self):
        return self._p4

    def set_p4(self, new_p4):
        self._p4 = new_p4

    def rescale_pt(self, pt_new):
        pt_old = self.p4().Pt()
        m_old = self.p4().M()
        p4_new = ROOT.TLorentzVector()
        p4_new.SetPtEtaPhiM(pt_new, self.p4().Eta(), self.p4().Phi(), pt_new/pt_old * m_old)
        self.set_p4(p4_new)

    def eta(self):
        return self.p4().Eta()

    def phi(self):
        return self.p4().Phi()

    def status(self):
        return self._status

    def parent_id(self):
        return self._parent_id

    def __str__(self):
        s = "Particle: {0:.4f} {1:.4f} {2:.4f} {3:.4f} {4} {5}".format(
            self.p4().Pt(), self.p4().Eta(), self.p4().Phi(), self.p4().M(),
            self.pdg_id(), self.parent_id()
        )
        return s

def make_jet_tf(mu_scale=1.0, sigma="1.0*sqrt([0])"):
    """Returns a gaussian PDF for the jet momentum f(ptgen | ptreco) with
    x: gen-pt
    par 0: reco-pt

    The resolution is encoded as 1.0/sqrt(x) -> 10% @ 100GeV
    
    Returns:
        TF1: transfer function
    """
    f = ROOT.TF1("jet_tf",
        "1.0/(sqrt(2 * {sigma} * {sigma} * 3.14159264)) * exp(-TMath::Power((x - {mu_scale}*[0]), 2)/(2*{sigma}*{sigma}))".format(
            mu_scale=mu_scale, sigma=sigma
        ),
        0, 500
    )
    f.SetNpx(100)
    f.SetMinimum(0)
    f.SetMaximum(500)
    return f

class EventRepresentation:
    """This summarizes the reco content of an event, which may be 'perturbed' under
    systematics.
    
    Attributes:
        leptons (list of Particle): Identified leptons in the event
        b_quarks (list of Particle): Identified b-quarks (or jets) in the event
        l_quarks (list of Particle): Identified light quarks (or jets) in the event
        invisible (TLorentzVector): MET of the event
    """
    def __init__(self, **kwargs):
        self.b_quarks = kwargs.get("b_quarks", [])
        self.l_quarks = kwargs.get("l_quarks", [])
        self.leptons = kwargs.get("leptons", [])
        self.invisible = kwargs.get("invisible", ROOT.TLorentzVector())

    def scale_quarks(self, sf=1.0):
        for q in self.b_quarks + self.l_quarks:
            pt_new = sf * q.p4().Pt()
            q.rescale_pt(pt_new)

    def smear_quarks(self, tf=None):
        """Changes the quark candidate momentum according by sampling
        from the associated transfer function.
        
        Args:
            tf (None, optional): TF1 with the quark transfer function.
        
        Returns:
            nothing
        """
        for q in self.b_quarks + self.l_quarks:
            if not tf is None:
                q.tf = tf
            pt_old = q.p4().Pt()
            q.tf.SetParameter(0, pt_old)
            pt_new = 0
            while np.isnan(pt_new) or pt_new == 0:
                pt_new = q.tf.GetRandom(0, 500)
            logging.debug("rescaling pt {0} -> {1}".format(q.p4().Pt(), pt_new))
            q.rescale_pt(pt_new)

    def smear_met(self, res=30):
        px = self.invisible.Px()
        py = self.invisible.Py()

        px_new = px + np.random.normal(0, res)
        py_new = py + np.random.normal(0, res)

        vec = ROOT.TLorentzVector()
        vec.SetPx(px_new)
        vec.SetPy(py_new)

        self.invisible = vec

    def perturb_flavour(self, effs={}):
        """Changes the jet-to-quark association according to b-tagging efficiencies in a random way
        
        Args:
            effs (dict, optional): Dictionary of per-flavour tagging efficiencies
        
        Returns:
            nothing
        """
        b_quarks = []
        l_quarks = []

        tag_cands = []
        for q in self.b_quarks:
            tag_cands += [(q, "b")]

        for q in self.l_quarks:
            tag_cands += [(q, "l")]

        for q, fl in tag_cands:
            #Checks if this jet would be b-tagged
            is_tagged = np.random.uniform() < effs[fl]
            if is_tagged:
                b_quarks += [q]
            else:
                l_quarks += [q]

        self.b_quarks = b_quarks
        self.l_quarks = l_quarks

    def make_interpretation(self, hypo, conf, selection_func):
        """Returns a MEM interpretation of the event, given a hypothesis.
        
        Args:
            hypo (string): MEM hypothesis
            conf (MEMConfig): MEM configuration
        
        Returns:
            TYPE: Description
        """
        if hypo == "0w2h2t":
            interp = EventInterpretation(
                b_quarks = self.b_quarks[:4],
                leptons = self.leptons,
                invisible = self.invisible,
                hypo = hypo,
                selection_function = selection_func
            )
        elif hypo == "1w2h2t":
            interp = EventInterpretation(
                b_quarks = self.b_quarks[:4],
                l_quarks = self.l_quarks[:2],
                leptons = self.leptons,
                invisible = self.invisible,
                hypo = hypo,
                selection_function = selection_func
            )
        elif hypo == "2w2h2t":
            interp = EventInterpretation(
                b_quarks = self.b_quarks[:4],
                l_quarks = self.l_quarks[:2],
                leptons = self.leptons,
                invisible = self.invisible,
                hypo = hypo,
                selection_function = selection_func
            )

        interp.mem_cfg = conf
        return interp

def apply_jet_cut(objs, conf):
    return filter(
        lambda x, conf=conf: x.p4().Pt() > conf["jets"]["pt"] and abs(x.p4().Eta()) < conf["jets"]["eta"],
        objs
    )
    
class GenQuarkLevelAnalyzer(Analyzer):
    """Processes the generated quarks in the event and creates a quark-level hypothesis 
    
    Attributes:
        conf (TYPE): Description
        logger (TYPE): Description
    """
    def __init__(self, cfg_ana, cfg_comp, looperName):
        super(GenQuarkLevelAnalyzer, self).__init__(cfg_ana, cfg_comp, looperName)
        self.conf = cfg_ana._conf
        self.logger = logging.getLogger("GenQuarkLevelAnalyzer")
        self.tf = make_jet_tf()
        
        self.mem_configs = {
            "default": EventInterpretation.setup_cfg_default(self.conf),
            "default": EventInterpretation.setup_cfg_default(self.conf),
        }
    def process(self, event):
        event.gen_b_h = map(lambda p: Particle.from_obj(p, 25), event.GenBQuarkFromH)
        for q in event.gen_b_h:
            q.source = "h"
            q.source_pdgId = 25
        event.gen_b_t = map(lambda p: Particle.from_obj(p, 6), event.GenBQuarkFromTop)
        for q in event.gen_b_t:
            q.source = "t"
            q.source_pdgId = 6
        event.gen_b_others = map(lambda p: Particle.from_obj(p, 0), event.GenStatus2bHad)
        for q in event.gen_b_others:
            q.source = "other"
            q.source_pdgId = 0
        event.gen_q_w = map(lambda p: Particle.from_obj(p, 24), event.GenWZQuark)
        for q in event.gen_q_w:
            q.source = "w"
            q.source_pdgId = 24

        event.gen_lep = map(lambda p: Particle.from_obj(p, 6), event.GenLepFromTop)
        event.gen_lep = filter(
            lambda x, conf=self.conf: x.p4().Pt() > conf["leptons"]["pt"] and abs(x.p4().Eta()) < conf["leptons"]["eta"],
            event.gen_lep
        )

        event.gen_nu = map(lambda p: Particle.from_obj(p, 6), event.GenNuFromTop)
        event.gen_met = sum([p.p4() for p in event.gen_nu], ROOT.TLorentzVector())
        self.logger.debug("process "
            "nGenBother={0} nGenBH={1} nGenBt={2} nGenLep={3}".format(
                len(event.gen_b_others), len(event.gen_b_h), len(event.gen_b_t), len(event.gen_lep)
            )
        )
        event.interpretations = {}

        ###
        ### b-quarks
        ###
        b_hadrons = event.gen_b_h + event.gen_b_t
       
        #invariant mass checks
        event.gen_h_p4 = sum([x.p4() for x in event.gen_b_h], ROOT.TLorentzVector())
        event.gen_w_p4 = sum([x.p4() for x in event.gen_q_w], ROOT.TLorentzVector())

        #Find b-hadrons in additional set that are not matched to existing higgs or top
        #derived b-hadrons
        additional_b_hadrons = []
        for bhad in event.gen_b_others:
            matches = matchObjectCollection(event.gen_b_others, b_hadrons, 0.3)
            if not matches.has_key(bhad) or matches[bhad] is None:
                self.logger.debug("no match for pt={0}, adding".format(bhad.p4().Pt()))
                bhad.source = "other"
                bhad.source_pdgId = 0
                b_hadrons += [bhad]
                additional_b_hadrons += [bhad]
        self.logger.debug("b from other: {0}".format(map(str, additional_b_hadrons)))

        event.gen_b_others_cleaned = additional_b_hadrons

        #Apply kinematic cuts and sort by pt
        event.b_hadrons_sorted = sorted(b_hadrons, key=lambda x: x.p4().Pt(), reverse=True)
       
        #add TF for quarks
        for q in event.b_hadrons_sorted + event.gen_q_w:
            q.tf = self.tf

        #create a quark-level representation
        event.repr_quarks = EventRepresentation(
            b_quarks = event.b_hadrons_sorted[:4],
            l_quarks = event.gen_q_w[:2],
            leptons = event.gen_lep,
            invisible = event.gen_met,
        )
        
        event.repr_quarks_smeared_jet = copy.deepcopy(event.repr_quarks)
        event.repr_quarks_smeared_jet.smear_quarks()

        #event.interpretations["gen_quark_dl_0w2h2t"] = event.repr_quarks.make_interpretation(
        #    "0w2h2t",
        #    self.mem_configs["default"],
        #    "dl_0w2h2t"
        #)
        #event.interpretations["gen_quark_sl_0w2h2t"] = event.repr_quarks.make_interpretation(
        #    "0w2h2t",
        #    self.mem_configs["default"],
        #    "sl_0w2h2t"
        #)
        #event.interpretations["gen_quark_sl_1w2h2t"] = event.repr_quarks.make_interpretation(
        #    "1w2h2t",
        #    self.mem_configs["default"],
        #    "sl_1w2h2t"
        #)
        event.interpretations["gen_quark_sl_2w2h2t"] = event.repr_quarks.make_interpretation(
            "2w2h2t",
            self.mem_configs["default"],
            "sl_2w2h2t"
        )
        #event.interpretations["gen_quark_sl_2w2h2t_smeared_jet"] = event.repr_quarks_smeared_jet.make_interpretation(
        #    "2w2h2t",
        #    self.mem_configs["default"],
        #    "sl_2w2h2t"
        #)

        return True

class GenJetLevelAnalyzer(Analyzer):
    """Processes the gen-jets in the event, matching them to quarks and
    creating jet-level event interpretations
    
    Attributes:
        conf (TYPE): Description
        logger (TYPE): Description
    """
    def __init__(self, cfg_ana, cfg_comp, looperName):
        super(GenJetLevelAnalyzer, self).__init__(cfg_ana, cfg_comp, looperName)
        self.conf = cfg_ana._conf
        self.logger = logging.getLogger("GenJetLevelAnalyzer")
        
        self.SAMPLE_TYPES = {
            "ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8": "sig",
            "TT_TuneCUETP8M2T4_13TeV-powheg-pythia8": "bkg",
        }

        self.tf = make_jet_tf()

        self.tf_res = {}
        self.tf_res["default"] = make_jet_tf()
        self.tf_res["fitted"] = make_jet_tf(mu_scale="(0.001*[0]+0.98)", sigma="(0.0013*[0]*[0] + 0.2*[0])")
        
        self.mem_configs = {
            "default": EventInterpretation.setup_cfg_default(self.conf),
        }

        self.jet_csv = open("jets.csv", "w")
        
    def beginLoop(self,setup):
        super(GenJetLevelAnalyzer,self).beginLoop(setup)
        outservice_name = "PhysicsTools.HeppyCore.framework.services.tfile.TFileService_outputfile"
        if outservice_name in setup.services :
            setup.services[outservice_name].file.cd()


        self.hist_ptratio_b = {}
        self.hist_ptratio_q = {}
        for res in self.tf_res.keys() + ["reco", "nosmear"]:
            n = "hb_res_{0}".format(res).replace(".", "_")
            self.hist_ptratio_b[res] = ROOT.TH2D(
                n,
                n,
                200, 0, 1000,
                200, 0, 4
            )

            n = "hq_res_{0}".format(res).replace(".", "_")
            self.hist_ptratio_q[res] = ROOT.TH2D(
                n,
                n,
                200, 0, 1000,
                200, 0, 4
            )


    def process(self, event):
    
        event.sample_type = self.SAMPLE_TYPES[self.cfg_comp.name.split("__")[-1]]

        #get all the gen-jet
        event.gen_jet = map(lambda p: Particle.from_obj(p, 0), event.GenJet)
        event.jet = map(lambda p: Particle.from_obj(p, 0), event.Jet)
   
        #match reco jets to gen jets based on deltaR (each gen jet will have 0-1 matched reco jets)
        matched_jet = matchObjectCollection(event.gen_jet, event.jet, 0.3)
        for jet in event.gen_jet:
            if matched_jet.has_key(jet) and matched_jet[jet] != None:
                jet.recoJet = matched_jet[jet]
            else:
                jet.recoJet = None

        #only apply gen-jet cuts now
        event.gen_jet = apply_jet_cut(event.gen_jet, self.conf)

        #jet-lepton cleaning
        matched_jet = matchObjectCollection(event.gen_jet, event.gen_lep, 0.3)
        cleaned_jets = []
        for jet in event.gen_jet:
            if matched_jet.has_key(jet) and matched_jet[jet] != None:
                self.logger.debug("removing jet with pt={0:.2f}".format(jet.p4().Pt()))
            else:
                cleaned_jets += [jet]
        event.gen_jet = cleaned_jets

        #assign transfer functions to jets
        for jet in event.gen_jet:
            jet.tf = self.tf

        #match b-quarks to gen-jets by deltaR
        matches = matchObjectCollection(event.gen_jet, event.b_hadrons_sorted, 0.3)
        
        #find jets matched to b-quarks
        event.matched_b_jets = []
        for jet in event.gen_jet:
            if matches.has_key(jet) and matches[jet] != None:
                p = matches[jet]
                #Assign the parent particle (b-quark) of the jet
                jet.source_p = p
                #assign the parent of the b-quark, e.g top or higgs
                jet.source = p.source
                jet._pdg_id = p.source_pdgId
                event.matched_b_jets += [jet]

        #find jets matched to light quarks
        matches = matchObjectCollection(event.gen_jet, event.gen_q_w, 0.3)
        event.matched_q_jets = []
        for jet in event.gen_jet:
            if matches.has_key(jet) and matches[jet] != None and jet not in event.matched_b_jets:
                p = matches[jet]
                #Assign the parent particle (light quark) of the jet
                jet.source_p = p
                #assign the parent of the light quark, e.g. W
                jet.source = p.source
                jet._pdg_id = p.source_pdgId
                event.matched_q_jets += [jet]

        #find unmatched jets
        event.unmatched_jets = []
        for jet in event.gen_jet:
            if jet not in event.matched_b_jets and jet not in event.matched_q_jets:
                event.unmatched_jets += [jet]

        #count number of jets matched to quarks from certain origin
        match_count = {
            "t": 0,
            "h": 0,
            "w": 0,
            "other": 0,
        }
        for jet in event.matched_b_jets + event.matched_q_jets:
            match_count[jet.source] += 1
        event.match_count = match_count

        self.logger.debug("process: matched_b_jets={0} matched_q_jets={0}".format(len(event.matched_b_jets), len(event.matched_q_jets)))

        #assign the gen-jets to b, q and unmatched based on geometrical matching
        #and the gen-level origin of the quark.
        #this means that the parent of the quark must come indeed from one of the 
        event.b_jets, event.q_jets, event.unmatched_jets = self.assign_b_q_jets(event.sample_type, event.gen_jet)
        event.b_jets = sorted(event.b_jets, key=lambda x: x.p4().Pt(), reverse=True)
        event.q_jets = sorted(event.q_jets, key=lambda x: x.p4().Pt(), reverse=True)
        event.unmatched_jets = sorted(event.unmatched_jets, key=lambda x: x.p4().Pt(), reverse=True)

        event.ngen_jet_b = len(event.b_jets)
        event.ngen_jet_q = len(event.q_jets)
        event.ngen_jet_u = len(event.unmatched_jets)

        #assign the event a gen-level category 
        event.category = self.assign_category(
            event.sample_type,
            event.ngen_jet_b,
            event.ngen_jet_q,
            match_count["h"],
            match_count["t"],
            match_count["w"]
        )

        #Take up to 4 b-quark candidates and 2 light quark candidates
        b_quark_cands = event.b_jets[:4]
        l_quark_cands = event.q_jets[:2]
        #if there are less than 4 b quark candidates, add one unmatched jet to b-quarks
        #this is there in order to be able to calculate 2h2t hypotheses even on events
        #that don't have fully matched b-quarks
        if len(b_quark_cands) < 4 and len(event.unmatched_jets) > 0:
            jet = event.unmatched_jets.pop(0)
            b_quark_cands += [jet]
            self.logger.debug("adding unmatched jet to b-quarks")
        #if there are less than 2 light quark candidates, add any missing ones, similar
        #to above
        while len(l_quark_cands) < 2 and len(event.unmatched_jets) > 0:
            jet = event.unmatched_jets.pop(0)
            l_quark_cands += [jet]
            self.logger.debug("adding unmatched jet to light quarks")

        #create the jet-based representation
        event.repr_jets = EventRepresentation(
            b_quarks = b_quark_cands,
            l_quarks = l_quark_cands,
            leptons = event.gen_lep,
            invisible = event.gen_met,
        )

        #Loop over gen-jets
        for jet in event.b_jets + event.q_jets:
            #make sure the gen-jet was associated to a reco jet
            if jet.recoJet is None:
                continue

            #get the corresponding quark pt
            pt_q = jet.source_p.p4().Pt()
            pt_gen = jet.p4().Pt()

            #fill the unsmeared 2D histogram with (pt_reco, pt_reco / pt_q) 
            if jet in event.b_jets:
                self.hist_ptratio_b["reco"].Fill(jet.p4().Pt(), jet.recoJet.p4().Pt() / pt_q)
            else:
                self.hist_ptratio_q["reco"].Fill(jet.p4().Pt(), jet.recoJet.p4().Pt() / pt_q)

            if jet in event.b_jets:
                self.hist_ptratio_b["nosmear"].Fill(jet.p4().Pt(), jet.p4().Pt() / pt_q)
            else:
                self.hist_ptratio_q["nosmear"].Fill(jet.p4().Pt(), jet.p4().Pt() / pt_q)
           
            self.jet_csv.write(
                ",".join([str(f) for f in [jet.p4().Pt(), abs(jet.p4().Eta()), pt_q, jet.recoJet.p4().Pt(), jet in event.b_jets]]) + 
                "\n"
            )
            #Loop over the various smearing functions
            for res in self.tf_res.keys():

                #smearing is a function of the quark pt
                self.tf_res[res].SetParameter(0, pt_gen)

                #smear the jet pt
                #need to do GetRandom within a limited range
                #as increasing the range increases smearing runtime considerably
                pt_new = self.tf_res[res].GetRandom(0, 500)
                #Fill a histogram with the 
                if jet in event.b_jets:
                    self.hist_ptratio_b[res].Fill(pt_new, pt_new / pt_q)
                else:
                    self.hist_ptratio_q[res].Fill(pt_new, pt_new / pt_q)
       
        #for iscale in [0.95, 0.96, 0.97, 0.98, 0.99, 1.01, 1.02, 1.03, 1.04, 1.05]:
        #    repr_jets_scaled = copy.deepcopy(event.repr_jets)
        #    repr_jets_scaled.scale_quarks(iscale)
        #    event.interpretations["gen_jet_sl_2w2h2t_scaled_jet_{0}".format(str(iscale).replace(".", "_"))] = repr_jets_scaled.make_interpretation(
        #        "2w2h2t",
        #        self.mem_configs["default"],
        #        "sl_2w2h2t"
        #    )

        
        #event.repr_jets_scaled_jet_down = copy.deepcopy(event.repr_jets)
        #event.repr_jets_scaled_jet_down.scale_quarks(0.98)

        #smear the jets / quark candidates
        event.repr_jets_smeared_jet = copy.deepcopy(event.repr_jets)
        event.repr_jets_smeared_jet.smear_quarks()

        #smear the MET
        #event.repr_jets_smeared_met = copy.deepcopy(event.repr_jets)
        #event.repr_jets_smeared_met.smear_met()

        #make various MEM interpretations
        #event.interpretations["gen_jet_dl_0w2h2t"] = event.repr_jets.make_interpretation(
        #    "0w2h2t",
        #    self.mem_configs["default"],
        #    "dl_0w2h2t"
        #)
        #event.interpretations["gen_jet_sl_0w2h2t"] = event.repr_jets.make_interpretation(
        #    "0w2h2t",
        #    self.mem_configs["default"],
        #    "sl_0w2h2t"
        #)
        #event.interpretations["gen_jet_sl_1w2h2t"] = event.repr_jets.make_interpretation(
        #    "1w2h2t",
        #    self.mem_configs["default"],
        #    "sl_1w2h2t"
        #)
        #
        event.interpretations["gen_jet_sl_2w2h2t"] = event.repr_jets.make_interpretation(
            "2w2h2t",
            self.mem_configs["default"],
            "sl_2w2h2t"
        )
        event.interpretations["gen_jet_sl_2w2h2t_smeared_jet"] = event.repr_jets_smeared_jet.make_interpretation(
            "2w2h2t",
            self.mem_configs["default"],
            "sl_2w2h2t"
        )
        #event.interpretations["gen_jet_sl_2w2h2t_smeared_met"] = event.repr_jets_smeared_met.make_interpretation(
        #    "2w2h2t",
        #    self.mem_configs["default"],
        #    "sl_2w2h2t"
        #)
        
        return True

    def assign_category(self, sample_type, nb, nq, nh, nt, nw):
        """
        Assigns a gen-level category to the event based on the sample type and
        the number of matched objects.
        sample_type: "sig" or "bkg"
        nb: number of b jets
        nq: number of q jets
        nh: number of jets matched to b-quarks from H
        nt: number of jets matched to b-quarks from top
        nw: number of jets matched to quarks from the W

        returns: a category string such as "2w2h2t" for a fully matched
            event, "1w2h2t" in case only one quark from the W could be matched
            etc.
        """
        if sample_type == "sig":
            if nw>=2:
                if nb>=4 and nh>=2 and nt>=2:
                    return "2w2h2t"
                elif nb>=3 and nh==1 and nt>=2:
                    return "2w1h2t"
                elif nb>=3 and nh>=2 and nt==1:
                    return "2w2h1t"
                else:
                    return "2w"
            elif nw==1:
                if nb>=4 and nh>=2 and nt>=2:
                    return "1w2h2t"
                elif nb==3 and nh==1 and nt>=2:
                    return "1w1h2t"
                elif nb==3 and nh>=2 and nt==1:
                    return "1w2h1t"
                else:
                    return "1w"
            elif nw==0:
                if nb>=4 and nh>=2 and nt>=2:
                    return "0w2h2t"
                elif nb>=3 and nh==1 and nt>=2:
                    return "0w1h2t"
                elif nb>=3 and nh>=2 and nt==1:
                    return "0w2h1t"
                else:
                    return "0w"
        elif sample_type == "bkg":
            if nw>=2:
                if nb>=4 and nt>=2:
                    return "2w2h2t"
                elif nb>=3 and nt>=2:
                    return "2w1h2t"
                elif nb>=3 and nt==1:
                    return "2w2h1t"
                else:
                    return "2w"
            elif nw==1:
                if nb>=4 and nt>=2:
                    return "1w2h2t"
                elif nb==3 and nt>=2:
                    return "1w1h2t"
                elif nb==3 and nt==1:
                    return "1w2h1t"
                else:
                    return "1w"
            elif nw==0:
                if nb>=4 and nt>=2:
                    return "0w2h2t"
                elif nb>=3 and nt>=2:
                    return "0w1h2t"
                elif nb>=3 and nt==1:
                    return "0w2h1t"
                else:
                    return "0w"
    
    def assign_b_q_jets_sig(self, gen_jets):
        """
        Given a set of gen jets, assign them to be either matched to
        b quarks or light quarks. The matching is based on whether
        the jets have ani associated b hadrons (from GenHFHadronMatcher)
        and geometrical matching to b and light quarks, for which we know
        the generator provenance.
        The matching is different for signal and background due to
        the existence of the higgs-matched b-quarks in signal.
        
        gen_jets: list of gen jets
        
        returns: tuple of identified b-jets, light jets and unmatched
            jets.
        """
        b_jets = []
        q_jets = []
        unmatched_jets = []
        for jet in gen_jets:
            if jet.numBHadrons>=1 and jet.source in ["t", "h"]:
                b_jets += [jet]
            elif jet.numBHadrons==0 and jet.source in ["w"]:
                q_jets += [jet]
            else:
                unmatched_jets += [jet]
        return b_jets, q_jets, unmatched_jets
    
    def assign_b_q_jets_bkg(self, gen_jets):
        """
        As the above function, but for background.
        """
        b_jets = []
        q_jets = []
        unmatched_jets = []
        for jet in gen_jets:
            if jet.numBHadrons>=1 and jet.source in ["t", "other"]:
                b_jets += [jet]
            elif jet.numBHadrons==0 and jet.source in ["w"]:
                q_jets += [jet]
            else:
                unmatched_jets += [jet]
        return b_jets, q_jets, unmatched_jets

    def assign_b_q_jets(self, sample_type, gen_jets):
        """
        Assign the gen-jets to b, q and unmatched based on the sample type.
        """
        if sample_type == "sig":
            return self.assign_b_q_jets_sig(gen_jets)
        elif sample_type == "bkg":
            return self.assign_b_q_jets_bkg(gen_jets)
        raise Exception("unknown sample type")

class OutputsAnalyzer(Analyzer):
    def __init__(self, cfg_ana, cfg_comp, looperName):
        super(OutputsAnalyzer, self).__init__(cfg_ana, cfg_comp, looperName)
        self.conf = cfg_ana._conf

    def process(self, event):
        for (k, v) in event.interpretations.items():
            setattr(event, "interp_" + k, v)
        return True


class MemoryAnalyzer(Analyzer):
    def __init__(self, cfg_ana, cfg_comp, looperName):
        super(MemoryAnalyzer, self).__init__(cfg_ana, cfg_comp, looperName)
        self.conf = cfg_ana._conf
        self.logger = logging.getLogger("MemoryAnalyzer")

    def process(self, event):
        memory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        if event.iEv % 100 == 0:
            self.logger.info("memory usage at event {0}: {1:.2f} MB".format(event.iEv, memory/1024.0))
        return True

match_type = NTupleObjectType("match_type", variables = [
    NTupleVariable("top", lambda x : x["t"], the_type=int),
    NTupleVariable("higgs", lambda x : x["h"], the_type=int),
    NTupleVariable("w", lambda x : x["w"], the_type=int),
    NTupleVariable("other", lambda x : x["other"], the_type=int),
])

tlorentzvector_type = NTupleObjectType("tlorentzvector_type", variables = [
    NTupleVariable("pt", lambda x : x.Pt()),
    NTupleVariable("eta", lambda x : x.Eta()),
    NTupleVariable("phi", lambda x : x.Phi()),
    NTupleVariable("m", lambda x : x.M())
])

if __name__ == "__main__":

    import PhysicsTools.HeppyCore.framework.config as cfg

    logging.basicConfig(level=logging.INFO)

    event_ana = cfg.Analyzer(
        EventAnalyzer,
        'events'
    )
    genquark_ana = cfg.Analyzer(
        GenQuarkLevelAnalyzer,
        'genquark',
        _conf = conf,
    )
    genjet_ana = cfg.Analyzer(
        GenJetLevelAnalyzer,
        'genjet',
        _conf = conf,
    )  
    mem_ana = cfg.Analyzer(
        MEMAnalyzer,
        'mem',
        _conf = conf,
    )
    outs_ana = cfg.Analyzer(
        OutputsAnalyzer,
        'outs',
        _conf = conf,
    )
    memory_ana = cfg.Analyzer(
        MemoryAnalyzer,
        'memory',
        _conf = conf,
    )

    AutoFillTreeProducer.fillCoreVariables = fillCoreVariables
    treeProducer = cfg.Analyzer(
        class_object = AutoFillTreeProducer,
        defaultFloatType = "F",
        verbose = True,
        vectorTree = True,
        globalVariables = [
            NTupleVariable("ttCls",  lambda ev: getattr(ev, "ttCls", -1), int,mcOnly=True, help="ttbar classification via GenHFHadronMatcher"),
            NTupleVariable("ngen_jet_b",  lambda ev: getattr(ev, "ngen_jet_b", -1), int,mcOnly=True, help=""),
            NTupleVariable("ngen_jet_q",  lambda ev: getattr(ev, "ngen_jet_q", -1), int,mcOnly=True, help=""),
            NTupleVariable("ngen_jet_u",  lambda ev: getattr(ev, "ngen_jet_u", -1), int,mcOnly=True, help=""),
            NTupleVariable("sample_type",  lambda ev: SAMPLE_CODE[getattr(ev, "sample_type")], int,mcOnly=False, help=""),
            NTupleVariable("category",  lambda ev: CATEGORY_CODE[getattr(ev, "category")], int, mcOnly=False, help=""),
        ],
        globalObjects = {
            #"interp_gen_quark_sl_0w2h2t" : NTupleObject("I_sl_0w2h2t_q", interp_type, help=""),
            #"interp_gen_quark_sl_1w2h2t" : NTupleObject("I_sl_1w2h2t_q", interp_type, help=""),
            "interp_gen_quark_sl_2w2h2t" : NTupleObject("I_sl_2w2h2t_q", interp_type, help=""),
            #"interp_gen_quark_sl_2w2h2t_smeared_jet" : NTupleObject("I_sl_2w2h2t_q_smeared_jet", interp_type, help=""),
            #"interp_gen_quark_dl_0w2h2t" : NTupleObject("I_dl_0w2h2t_q", interp_type, help=""),
            #"interp_gen_jet_sl_0w2h2t" : NTupleObject("I_sl_0w2h2t_j", interp_type, help=""),
            #"interp_gen_jet_sl_1w2h2t" : NTupleObject("I_sl_1w2h2t_j", interp_type, help=""),
            "interp_gen_jet_sl_2w2h2t" : NTupleObject("I_sl_2w2h2t_j", interp_type, help=""),
            "interp_gen_jet_sl_2w2h2t_smeared_jet" : NTupleObject("I_sl_2w2h2t_j_smeared_jet", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_0_95" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_0_95", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_0_96" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_0_96", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_0_97" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_0_97", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_0_98" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_0_98", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_0_99" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_0_99", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_1_01" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_1_01", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_1_02" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_1_02", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_1_03" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_1_03", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_1_04" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_1_04", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_scaled_jet_1_05" : NTupleObject("I_sl_2w2h2t_j_scaled_jet_1_05", interp_type, help=""),
            #"interp_gen_jet_sl_2w2h2t_smeared_met" : NTupleObject("I_sl_2w2h2t_j_smeared_met", interp_type, help=""),
            #"interp_gen_jet_dl_0w2h2t" : NTupleObject("I_dl_0w2h2t_j", interp_type, help=""),
            "match_count" : NTupleObject("match", match_type, help="gen-jet matching"),
            "gen_h_p4" : NTupleObject("gen_h_p4", tlorentzvector_type, help=""),
            "gen_w_p4" : NTupleObject("gen_w_p4", tlorentzvector_type, help=""),
        },
        collections = {
            "gen_b_h" : NTupleCollection("gen_b_h", genParticleType, 4, help="generated quarks from H"),
            "gen_b_t" : NTupleCollection("gen_b_t", genParticleType, 4, help="generated quarks from top"),
            "gen_b_others_cleaned" : NTupleCollection("gen_b_others", genParticleType, 4, help="generated b-quarks from other sources"),
            "gen_q_w" : NTupleCollection("gen_q_w", genParticleType, 8, help="generated quarks from W/Z"),
            "b_hadrons_sorted" : NTupleCollection("gen_b", genParticleType, 4, help="all chosen generated b-quarks"),

            "gen_lep" : NTupleCollection("gen_lep", genParticleType, 4, help="generated leptons after selection"),
            "gen_jet": NTupleCollection("gen_jet", genJetType, 20, help="generated jets after selection"),
#            "matched_b_jets" : NTupleCollection("gen_jets_b", genJetType, 8, help="gen-jets matched to b-quarks"),
#            "matched_q_jets" : NTupleCollection("gen_jets_q", genJetType, 8, help="gen-jets matched to light quarks"),
#            "unmatched_jets" : NTupleCollection("gen_jets_unmatched", genJetType, 8, help="gen-jets not matched to quarks"),
        },
    )

    sequence = cfg.Sequence([
        event_ana,
        genquark_ana,
        genjet_ana,
        mem_ana,
        outs_ana,
        memory_ana,
        treeProducer
    ])

    from PhysicsTools.HeppyCore.framework.services.tfile import TFileService 
    output_service = cfg.Service(
        TFileService,
        'outputfile',
        fname='tree.root',
        option='recreate'
    )

    if os.environ.has_key("FILE_NAMES"):
        fns = os.environ["FILE_NAMES"].split()
        fns = map(getSitePrefix, fns)
        dataset = os.environ["DATASETPATH"]
        firstEvent = int(os.environ["SKIP_EVENTS"])
        nEvents = int(os.environ["MAX_EVENTS"])
    elif os.environ.get("TTH_SAMPLE", "tth") == "tth":
        fns = map(getSitePrefix, [
            "/store/user/jpata/tth/Mar1_mem/ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8/Mar1_mem/170301_161815/0000/tree_{0}.root".format(i)
            for i in range(1,10)
        ])
        dataset = "ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8"
        firstEvent = 0
        nEvents = 200
    elif os.environ.get("TTH_SAMPLE", "tth") == "ttjets":
        fns = map(getSitePrefix, [
            "/store/user/jpata/tth/Mar1_mem/TT_TuneCUETP8M2T4_13TeV-powheg-pythia8/Mar1_mem/170301_165949/0000/tree_{0}.root".format(i)
            for i in range(1,10)
        ])
        dataset = "TTToSemilepton_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8"
        firstEvent = 0
        nEvents = 1000

    "files", fns
    config = cfg.Config(
        #Run across these inputs
        components = [cfg.MCComponent(
            dataset,
            files = fns,
            tree_name = "vhbb/tree",
        )],
        sequence = sequence,
        services = [output_service],
        events_class = Events,
    )
    from PhysicsTools.HeppyCore.framework.looper import Looper
    looper = Looper(
        'Loop',
        config,
        nPrint = 0,
        firstEvent = firstEvent,
        nEvents = nEvents,
    )
    looper.loop()
    looper.write()

    #tf = ROOT.TFile.Open("{0}/tree.root".format(looper.name), "UPDATE")
    #for fn in fns:
    #    inf = ROOT.TFile.Open(fn)
    #    hcount = inf.Get("vhbb/Count")
    #    tf.cd()
    #    if tf.Get("Count") == None:
    #        hcount.Clone()
    #    else:
    #        tf.Get("Count").Add(hcount)
    #    inf.Close()
