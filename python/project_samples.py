import ROOT
import multiprocessing, os
from TTH.MEAnalysis.ParHadd import hadd 

#ttbar heavy-light classification
ttjets_sel = [
    ("ttb", "ttCls == 51"),
    ("tt2b", "ttCls >= 52"),
    ("ttOther", "ttCls <= 50")
]


#list of filename -> selection that you want to project
inf = [
    ("test/batch/ttjets.root", ttjets_sel),
]

def chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]

def SelectTree(infile, outfile, treename, cut, firstEntry=0, numEntries=-1):
    tf = ROOT.TFile(infile)
    otf = ROOT.TFile(outfile, "RECREATE")
    otf.cd()
    if numEntries < 0:
        numEntries = tf.Get(treename).GetEntries()
    print cut, numEntries, firstEntry
    t2 = tf.Get(treename).CopyTree(cut, "", numEntries, firstEntry)
    if not t2:
        raise Exception("could not project tree with cut {0}".format(cut))
    t2.Write()
    ni = t2.GetEntries()
    otf.Close()
    tf.Close()

def SelectTree_par(args):
    return SelectTree(*args)

pool = multiprocessing.Pool(10)

for infile, cuts in inf:
    tf = ROOT.TFile(infile)
    Ntree = tf.Get("tree").GetEntries()
    chunksize = 500000 
    for cn, cut in cuts:
        outfile = infile.replace(".root", "_{0}.root".format(cn))
        
        arglist = []
        ichunk = 0
        filenames = []
        for ch in chunks(range(Ntree), chunksize):
            _ofn = outfile + "." + str(ichunk)
            filenames += [_ofn] 
            arglist += [(infile, _ofn, "tree", cut, ch[0], len(ch))] 
            ichunk += 1 
        print "mapping", infile, cn
        pool.map(SelectTree_par, arglist)
        print "Merging to", outfile
        hadd((outfile, filenames))
        for fn in filenames:
            os.remove(fn)
