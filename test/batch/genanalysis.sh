#!/bin/bash

if [ -z "$GC_SCRATCH" ]; then
    GC_SCRATCH=`mktemp -d`
fi

set -e
source /cvmfs/cms.cern.ch/cmsset_default.sh
source env.sh
cd $CMSSW_BASE
eval `scram runtime -sh`


cd $GC_SCRATCH
python $CMSSW_BASE/src/TTH/GenLevel/python/genLevelAnalysis_cms.py
mv Loop/tree.root ./
